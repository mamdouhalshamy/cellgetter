package com.kaf.mms.cellgetter;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.kaf.mms.cellgetter.R.id;

public class MainActivity extends Activity implements OnClickListener {

	/* Ui Refs */
	TextView CidTextView;
	TextView LACidTextView;
	TextView MNCTextView;
	TextView MCCTextView;
	TextView longTextView;
	TextView latTextView;
	
	EditText CellNameEdt;
	Button SetCellNameBtn;
	
	Button getBtn;
	ToggleButton ServiceControllerTBtn;

	CellService mCellService;

	boolean mIsBound;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		initRefs();
	}

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the service object we can use to
			// interact with the service. Because we have bound to a explicit
			// service that we know is running in our own process, we can
			// cast its IBinder to a concrete class and directly access it.
			mCellService = ((CellService.LocalBinder) service).getService();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mCellService = null;
		}
	};

	void doBindService() {
		// Establish a connection with the service. We use an explicit
		// class name because we want a specific service implementation that
		// we know will be running in our own process (and thus won't be
		// supporting component replacement by other applications).
		Log.i("CellGetter", ">>Service Binding");
		bindService(new Intent(this, CellService.class), mServiceConnection,
				Context.BIND_AUTO_CREATE);
		mIsBound = true;
	}

	void doUnbindService() {
		if (mIsBound) {
			// Detach our existing connection.
			unbindService(mServiceConnection);
			mIsBound = false;
		}
	}

	void initRefs() {
		CidTextView = (TextView) findViewById(id.Cid);
		LACidTextView = (TextView) findViewById(id.LACid);

		MNCTextView = (TextView) findViewById(id.mnc);
		MCCTextView = (TextView) findViewById(id.mcc);

		longTextView = (TextView) findViewById(id.longitude);
		latTextView = (TextView) findViewById(id.latitude);
		
		getBtn = (Button) findViewById(id.get);
		getBtn.setOnClickListener(this);

		ServiceControllerTBtn = (ToggleButton) findViewById(id.ServiceController);
		ServiceControllerTBtn.setOnClickListener(this);
		
		CellNameEdt = (EditText)findViewById(id.CellName);
		SetCellNameBtn = (Button)findViewById(id.setCellNameBtn);
		SetCellNameBtn.setOnClickListener(this);
		

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {

		case id.get:
			if (mCellService != null) {
				CidTextView.setText(mCellService.getLastCellId());
				LACidTextView.setText(mCellService.getLastLAC());
			}

			break;

		case id.ServiceController:
			if(ServiceControllerTBtn.isChecked())
				doBindService();
			else
				doUnbindService();
			break;

		case id.setCellNameBtn:
			mCellService.setCellName(CellNameEdt.getText().toString());
			break;

		default:
			break;
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		doUnbindService();
	}

}