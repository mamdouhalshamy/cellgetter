package com.kaf.mms.cellgetter;

/*gets CellId, LAC and save it in XML*/

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Binder;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

public class CellService extends Service {

	Xmler xmler;
	GsmCellLocation GSMCellLocationObject;
	TelephonyManager TelephonyManagerObject;
	LocationManager LocationManagerObject;

	Timer timer = new Timer();
	
	private NotificationManager mNM;
	private int NOTIFICATION = R.string.Cell_service_started;
	
	String CellId = "N/A";
	String prevCellId = "N/A";	//made 2 reduce generated file size if data is redundant
	String LAC = "N/A";
	String MCC = "N/A";
	String MNC = "N/A";
	
	String CellName = "--";

	@Override
	public void onCreate() {
		super.onCreate();
		
		Log.i("CellGetter", ">>Starting Service");
		
		mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		showNotification();
		
		GSMCellLocationObject = new GsmCellLocation();
		TelephonyManagerObject = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

		xmler = new Xmler();

		xmlPostInfo();

		pollOnCellId();

	}

	private void pollOnCellId() {
		timer.scheduleAtFixedRate(new TimerTask() {;

			@Override
			public void run() {
				Log.i("CellGetter", ">>Getting Cell info");
				/* Cid and LAC */
				GSMCellLocationObject = (GsmCellLocation) TelephonyManagerObject
						.getCellLocation();
				if (GSMCellLocationObject.getCid() != -1)
					CellId = Integer.toString(GSMCellLocationObject.getCid());

				if (GSMCellLocationObject.getLac() != -1)
					LAC = Integer.toString(GSMCellLocationObject.getLac());

				/* MCC MNC */
				String networkOperator = TelephonyManagerObject
						.getNetworkOperator();
				if (networkOperator != null && networkOperator.length() > 0) {
					try {
						MCC = networkOperator.substring(0, 3);
						MNC = networkOperator.substring(3);
					} catch (NumberFormatException e) {
					}
				}
				
				Log.i("CellGetter", "CellId: " + CellId);
				Log.i("CellGetter", "LAC" + LAC);

				/* long and lat */
				// LocationManagerObject = (LocationManager)
				// getSystemService(Context.LOCATION_SERVICE);
				// LocationManagerObject.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				// 60000,
				// 1, this);

				try {
					Log.i("CellGetter", ">> Writing CellId n LAC to XML");
//					Log.i("CellGetter", ">> CellName is : " + CellName);
					
					if(!prevCellId.equals(CellId)) //saving file size 4 redundant data
						xmler.setPosition(CellName, CellId, LAC);
					prevCellId = CellId;
					CellName = "--"; //Clear CellName
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalStateException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

			}
		}, 0, Long.parseLong(getResources().getString(
				R.string.CellIdPollingInterval)));
	}

	private void xmlPostInfo() {
	}
	
	private void showNotification() {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = "ON";

        // Set the icon, scrolling text and timestamp
        Notification notification = new Notification(R.drawable.tower, text,
                System.currentTimeMillis());

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        // Set the info for the views that show in the notification panel.
        notification.setLatestEventInfo(this, "CellGetter",
                       text, contentIntent);

        // Send the notification.
        mNM.notify(NOTIFICATION, notification);
    }

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (timer != null)
			{
			timer.cancel();
			Log.i("CellGetter", ">>Closing Cell info Getting Timer");
			}

		try {
			Log.i("CellGetter", ">> Closing XML file");
			xmler.endXml();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		mNM.cancelAll();
	}

	private final IBinder mBinder= new LocalBinder();
	
	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}
	
	public class LocalBinder extends Binder {
		CellService getService() {
			return CellService.this;
		}
	}
	
	public String getLastCellId()
	{
		return CellId;
	}
	
	public String getLastLAC()
	{
		return LAC;
	}
	
	public void setCellName(String CellName)
	{
		Log.i("CellGetter", "passed cellname: " + CellName);
		this.CellName = CellName;
	}

}
