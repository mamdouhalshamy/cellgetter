package com.kaf.mms.cellgetter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.xmlpull.v1.XmlSerializer;

import android.os.Environment;
import android.util.Log;
import android.util.Xml;

public class Xmler {

	FileOutputStream fileos;
	final XmlSerializer serializer;

	public Xmler() {

		// creating xml file with naming of current time
		SimpleDateFormat s = new SimpleDateFormat("dd_MM_yyyy_hhmm");
		String date = s.format(new Date());
		File xmlfile = createXmlFile(date);

		// we have to bind the new file with a FileOutputStream
		fileos = null;
		try {
			fileos = new FileOutputStream(xmlfile);
		} catch (FileNotFoundException e) {
			Log.e("FileNotFoundException", "can't create FileOutputStream");
		}
		this.serializer = Xml.newSerializer();

		try {
			// we set the FileOutputStream as output for the serializer, using
			// UTF-8 encoding
			this.serializer.setOutput(fileos, "UTF-8");
			// Write <?xml declaration with encoding (if encoding not null) and
			// standalone flag (if standalone not null)
			this.serializer.startDocument(null, Boolean.valueOf(true));
			// set indentation option
			this.serializer.setFeature(
					"http://xmlpull.org/v1/doc/features.html#indent-output",
					true);
			// start a tag called "root"
			this.serializer.startTag(null, "root");

		} catch (Exception e) {
		}

	}

	public void setPosition(String positionName, String CellId, String LAC)
			throws IllegalArgumentException, IllegalStateException, IOException {

		serializer.startTag(null, "position");
		
		serializer.attribute(null, "name", positionName);

		//time
		serializer.startTag(null, "time");
		SimpleDateFormat st = new SimpleDateFormat("hh:mm:ss");
		String time = st.format(new Date());
		serializer.text(time);
		serializer.endTag(null, "time");

		//CellId
		serializer.startTag(null, "CellId");
		serializer.text(CellId);
		serializer.endTag(null, "CellId");
		
		//LAC
		serializer.startTag(null, "LAC");
		serializer.text(LAC);
		serializer.endTag(null, "LAC");

		serializer.endTag(null, "position");
	}

	private File createXmlFile(String name) {
		File CellGetterDir = new File(Environment.getExternalStorageDirectory()
				, "/CellGetter/");
		CellGetterDir.mkdirs();
		
		File xmlfile = new File(CellGetterDir, name + ".xml");
		
		try {
			xmlfile.createNewFile();
		} catch (IOException e) {
			Log.e("IOException", "exception in createNewFile() method");
		}

		return xmlfile;
	}

	public void endXml() throws IllegalArgumentException,
			IllegalStateException, IOException {
		serializer.endTag(null, "root");
		serializer.endDocument();
		// write xml data into the FileOutputStream
		serializer.flush();
		// finally we close the file stream
		fileos.close();

		// file created
	}
}
